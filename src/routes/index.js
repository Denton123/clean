var express = require("express");
var router = express.Router();

const makeExpressCallback = require("../express-callback/app");
const middleware = require("../middlewares/middleware");
const { upload } = require("../middlewares/upload");

upload.single("file");

//USER
const {
  _getusers,
  _createuser,
  _updateuser,
} = require("../controllers/users/app");

//ROLES
const {
  _getroles,
  _createroles,
  _updaterole,
} = require("../controllers/role/app");

//INVENTORY
const {
  _getinventory,
  _createinventory,
  _updateinventory,
} = require("../controllers/inventory/app");

//SUPPLIERS
const {
  _getsupplier,
  _createsupplier,
  _updatesupplier,
} = require("../controllers/supplier/app");

//delivery
const {
  _getdelivery,
  _createdelivery,
} = require("../controllers/delivery/app");

//sales
const { _getsale, _createsale } = require("../controllers/salestrans/app");

// // ------------------------------Routes---------------------------------
//GET
router.get("/users", makeExpressCallback(_getusers));
router.get("/roles", makeExpressCallback(_getroles));
router.get("/inventory", makeExpressCallback(_getinventory));
router.get("/suppliers", makeExpressCallback(_getsupplier));
router.get("/delivery", makeExpressCallback(_getdelivery));
router.get("/sales", makeExpressCallback(_getsale));

//post
router.post("/roles", makeExpressCallback(_createroles));
router.post("/inventory", makeExpressCallback(_createinventory));
router.post("/suppliers", makeExpressCallback(_createsupplier));
router.post("/delivery", makeExpressCallback(_createdelivery));
router.post("/sales", makeExpressCallback(_createsale));
router.post("/user", makeExpressCallback(_createuser));

//update
router.put("/roles/:role_id", makeExpressCallback(_updaterole));
router.put("/suppliers/:supplier_id", makeExpressCallback(_updatesupplier));
router.put("/inventory/:inventory_id", makeExpressCallback(_updateinventory));
router.put("/user/:users_id", makeExpressCallback(_updateuser));

module.exports = router;
