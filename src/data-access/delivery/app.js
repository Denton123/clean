const pool = require("../../config/config");
const moment = require("moment");

const DeliveryDB = {
  finddelivery: async (dr_no) => {
    return await pool.query(
      "SELECT * FROM delivery_transactions where dr_no=$1",
      [dr_no]
    );
  },

  getalldelivery: async () => {
    const dt = await pool.query(
      `SELECT dt.dt_id,
              dt.dr_no, 
              s.company_name as supplier_name,
              dt.dtransaction_date as transaction_date, 
              dt.total_cost
              FROM delivery_transactions dt
              LEFT JOIN suppliers s ON s.supplier_id = dt.supplier_id
      
              ORDER BY dt.dt_id
       `
    );
    // Loop through all results
    for (let i = 0; i < dt.rows.length; i++) {
      // Get sales transaction per index
      const element = dt.rows[i];
      // Get all items based on OR number of current index
      const dti = await pool.query(
        `  SELECT dti.dti_id,
                  dti.dt_no,
                  i.barcode,
                  i.product_description, 
                  dti.unit_cost,
                  dti.quantity
                   FROM delivery_transactions_items dti
                  LEFT JOIN inventory i ON dti.inventory_id = i.inventory_id 
                   WHERE dti.dt_no = $1
                  ORDER BY dti.dti_id
           `,
        [element.dr_no]
      );

      // Insert items on sales transaction based on index
      if (dt.rows[i]) {
        dt.rows[i].items = dti.rows;
      }
    }
    return dt.rows;
  },

  //create suppliers
  createdelivery: async (DeliveryInfo, res) => {
    const {
      supplier_id,
      total_cost,
      dr_no,
      dt_no,
      items,
      created_by,
    } = DeliveryInfo;

    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const dt = await pool.query(
      "INSERT INTO delivery_transactions (dr_no, supplier_id, dtransaction_date, total_cost ,created_at, created_by) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *",
      [dr_no, supplier_id, today, total_cost, today, created_by]
    );

    const ditems = []

    for (let i = 0; i < items.length; i++) {
      const { inventory_id, quantity, cost_per_unit } = items[i];
      const dti = await pool.query(
        `INSERT INTO delivery_transactions_items (dt_no, inventory_id,unit_cost, quantity) VALUES ($1, $2, $3, $4) RETURNING *`,
        [dr_no, inventory_id, cost_per_unit, quantity]
      );

      ditems.push(dti.rows[0])
    }

    dt.rows[0].items = ditems;

    return dt
  },
};
module.exports = { DeliveryDB };
