const pool = require("../../config/config")
const moment = require("moment");
 
const saleDB = {
    getallsale: async () => {
      const st = await pool.query(
        `SELECT st.st_id,
                st.or_no, 
                st.stransaction_date,
                st.total_cost,
                st.payment_amt,
                st.customer_name,
                st.customer_address,
                st.customer_contact_no,
                st.created_at,
                st.created_by
            FROM sales_transactions st
     `
      );
    
      // Loop through all results
      for (let i = 0; i < st.rows.length; i++) {
        // Get sales transaction per index
        const element = st.rows[i];
        // Get all items based on OR number of current index
        const sti = await pool.query(
          ` SELECT sti.sti_id,
                  sti.st_id,
                  i.inventory_id,
                  i.barcode,
                  i.product_description, 
                  sti.quantity,
                  sti.sales_cost
                  FROM sales_transactions_items sti
                  LEFT JOIN inventory i ON sti.inventory_id = i.inventory_id 
              WHERE sti.st_id = $1
              ORDER BY sti.sti_id`,
          [element.or_no]
        );
    
        // Insert items on sales transaction based on index
        if (st.rows[i]) {
          st.rows[i].items = sti.rows;
        }
      }
    
      return st.rows
    },

    createsales: async (saleInfo, res) => {
      const {
        total_cost,
        payment_amt,
        customer_name,
        customer_address,
        customer_contact_no,
        items,
        created_by,
      } = saleInfo;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
      const st = await pool.query(
        `INSERT INTO sales_transactions (total_cost, payment_amt, customer_name, customer_address ,customer_contact_no, created_by,created_at,stransaction_date) 
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *`,
        [
          total_cost,
          payment_amt,
          customer_name,
          customer_address,
          customer_contact_no,
          created_by,
          today,
          today,
        ]
      );
      const sitems = []
      for (let i = 0; i < items.length; i++) {
        const { inventory_id, quantity, sales_cost } = items[i];
      const st_id = st.rows[0].st_id;
      if (!isNaN(inventory_id) && !isNaN(quantity) && !isNaN(sales_cost)) {
        const query = `INSERT INTO sales_transactions_items(st_id, inventory_id, sales_cost, quantity) VALUES (${st_id}, ${inventory_id}, ${sales_cost}, ${quantity})`;
        await pool.query(query);

        // await pool.query(
        //   'SELECT or_no as dt_no FROM sales_transactions_items'

        await pool.query(
          `UPDATE inventory SET quantity = quantity - $1 WHERE inventory_id = $2 `,
          [quantity, inventory_id]
        );
      }
      }
      st.rows[0].items = items;
      return st
    }




}
module.exports = { saleDB }