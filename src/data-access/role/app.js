const { response } = require("express");
const pool = require("../../config/config");

roleDb = {
  findrole: async (role_name) => {
    return await pool.query("SELECT * FROM roles where role_name=$1", [
      role_name,
    ]);
  },

  listAccounts: async (roleInfo) => {
    return await pool.query("SELECT * FROM roles");
  },

  createrole: async (roleInfo, res) => {
    console.log(roleInfo);
    const { role_name, created_at, created_by } = roleInfo;
    // console.log(accountInfo)
    return await pool.query(
      "INSERT INTO roles(role_name,created_at,created_by) VALUES($1,$2,$3) RETURNING *",
      [role_name, created_at, created_by]
    );
  },

  updaterole: async (updateroleinfo, role_id) => {
    const { role_name, updated_at, updated_by } = updateroleinfo;
    return await pool.query(
      "UPDATE roles SET  role_name= $1,updated_at =$2,updated_by =$3 WHERE role_id=$4 RETURNING *",
      [role_name, updated_at, updated_by, role_id]
    );
  },
};
module.exports = { roleDb };
