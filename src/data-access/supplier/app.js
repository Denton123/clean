const pool = require("../../config/config");

const supplierDB = {
  findsupplier: async (company_name) => {
    return await pool.query("SELECT * FROM suppliers where company_name=$1", [
      company_name,
    ]);
  },

  listAccounts: async (info) => {
    return await pool.query("SELECT * FROM suppliers");
  },

  createsupplier: async (supplierinfo, res) => {
    const {
      company_name,
      contact_no,
      company_address,
      status,
      created_at,
      created_by,
    } = supplierinfo;
    return await pool.query(
      "INSERT INTO suppliers ( company_name, contact_no,company_address,status,created_at,created_by) VALUES ($1, $2,$3,$4,$5,$6)RETURNING *",
      [
        company_name,
        contact_no,
        company_address,
        status,
        created_at,
        created_by,
      ]
    );
  },

  updatesupplier: async (updatesupplierinfo, supplier_id) => {
    const {
      company_name,
      contact_no,
      company_address,
      status,
      updated_by,
      updated_at,
    } = updatesupplierinfo;
    return await pool.query(
      "UPDATE suppliers SET company_name = $1,contact_no = $2 ,company_address=$3, status=$4 ,updated_at =$5,updated_by =$6 WHERE supplier_id= $7 RETURNING *",
      [
        company_name,
        contact_no,
        company_address,
        status,
        updated_at,
        updated_by,
        supplier_id,
      ]
    );
  },
};

module.exports = { supplierDB };
