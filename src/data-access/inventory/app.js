const pool = require("../../config/config");

const invenDb = {
  findinvo: async (barcode) => {
    return await pool.query("SELECT * FROM inventory where barcode = $1", [
      barcode,
    ]);
  },

  listAccounts: async (info) => {
    return await pool.query("SELECT * FROM inventory");
  },

  createinventory: async (invfo, res) => {
    const {
      barcode,
      product_description,
      quantity,
      unit_cost,
      sales_cost,
      created_at,
      created_by,
    } = invfo;
    // console.log(accountInfo)
    return await pool.query(
      "INSERT INTO inventory(barcode,product_description,quantity,unit_cost,sales_cost,created_at,created_by) VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING *",

      [
        barcode,
        product_description,
        quantity,
        unit_cost,
        sales_cost,
        created_at,
        created_by,
      ]
    );
  },

  updateinventory: async (inventoryinfo, inventory_id) => {
    const {
      product_description,
      barcode,
      quantity,
      unit_cost,
      sales_cost,
      updated_by,
      updated_at,
    } = inventoryinfo;
    return await pool.query(
      "UPDATE inventory SET product_description =$1,barcode=$2,quantity=$3,unit_cost=$4,sales_cost=$5,updated_by=$6,updated_at=$7 WHERE inventory_id= $8 RETURNING *",
      [
        product_description,
        barcode,
        quantity,
        unit_cost,
        sales_cost,
        updated_by,
        updated_at,
        inventory_id,
      ]
    );
  },
};
module.exports = { invenDb };
