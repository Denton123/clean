const pool = require("../../config/config");
const bcrypt = require("bcrypt");

const accountDb = {
  findname: async (username) => {
    return await pool.query("SELECT * FROM users where username=$1", [
      username,
    ]);
  },

  findemployeeCode: async (employee_code) => {
    return await pool.query("SELECT * FROM users where employee_code=$1", [
      employee_code,
    ]);
  },

  listAccounts: async (info) => {
    return await pool.query("SELECT * FROM users");
  },

  createusers: async (accountInfo, res) => {
    const {
      password,
      username,
      employee_code,
      created_at,
      role_id,
      created_by,
    } = accountInfo;

    const salt = await bcrypt.genSalt(10);
    const bcryptPassword = await bcrypt.hash(password, salt);
    await pool.query(
      "INSERT INTO users (employee_code,username, password, role_id, created_by, created_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *",
      [employee_code, username, bcryptPassword, role_id, created_by, created_at]
    );
    return accountInfo;
  },

  updateusers: async (userinfo, users_id) => {
    const { username, password, role_id, updated_by, updated_at } = userinfo;

    const salt = await bcrypt.genSalt(10);
    const bcryptPassword = await bcrypt.hash(password, salt);
    if (!bcryptPassword) {
      res.status(400).json({ msg: "Please Input updated_at" });
    }
    await pool.query(
      "UPDATE users SET username =$1,password =$2,role_id=$3,updated_by=$4,updated_at=$5 WHERE users_id= $6 RETURNING *",
      [username, bcryptPassword, role_id, updated_by, updated_at, users_id]
    );

    return userinfo;
  },
};
module.exports = { accountDb };
