const updateusers = ({}) => {
  return function updateusername({
    username,
    password,
    role_id,
    updated_by,
    updated_at,
  }) {
    if (!username) {
      throw new Error("Please Input username");
    }
    if (!password) {
      throw new Error("Please input password");
    }

    if (!role_id) {
      throw new Error("Please input role_id");
    }

    return Object.freeze({
      updatename: () => username,
      updatePassword: () => password,
      updateRole: () => role_id,
      updateBY: () => updated_by,
      updateAt: () => updated_at,
    });
  };
};

module.exports = updateusers;
