const createusers = ({}) => {
  return function makeuser({
    employee_code,
    username,
    password,
    role_id,
    created_at,
    created_by,
  }) {
    if (!employee_code) {
      throw new Error("Please input employee_code");
    }

    if (!username) {
      throw new Error("Please input username");
    }

    if (!password) {
      throw new Error("Please input password");
    }

    if (!role_id) {
      throw new Error("Please input role_id");
    }

    if (!created_at) {
      throw new Error("Please input created_at");
    }

    if (!created_by) {
      throw new Error("Please input created_by");
    }

    return Object.freeze({
      createEmployeeCode: () => employee_code,
      createUsename: () => username,
      createPassword: () => password,
      roleID: () => role_id,
      createdAt: () => created_at,
      createdBy: () => created_by,
    });
  };
};

module.exports = createusers;
