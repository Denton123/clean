const getuserlist = require("./users");
const createusers = require("./create-user");
const updateusers = require("./update-user");

const getuser = getuserlist({});
const makeuser = createusers({});
const updateusername = updateusers({});

const accountEntity = Object.freeze({
  getuser,
  makeuser,
  updateusername,
});

module.exports = accountEntity;
module.exports = {
  getuser,
  makeuser,
  updateusername,
};
