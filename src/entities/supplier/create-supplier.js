const createsuppliers = ({}) => {
  return function makesupplier({
    company_name,
    contact_no,
    company_address,
    status,
    created_at,
    created_by,
  }) {
    if (!company_name) {
      throw new Error("Please Input company_name");
    }
    if (!contact_no) {
      throw Error("Please input contact_no");
    }

    if (!company_address) {
      throw new Error("Please input company_address");
    }

    if (!status) {
      throw new Error("Please input status");
    }

    if (!created_at) {
      throw new Error("Please input created_at");
    }

    if (!created_by) {
      throw new Error("Please input created_by");
    }

    
    return Object.freeze({
      GetCompanyName: () => company_name,
      GetContactNum: () => contact_no,
      GetContactAddress: () => company_address,
      GetStatus: () => status,
      GetCreatedAt:() => created_at,
      GetCreatedBy:() => created_by,
    
  })
}
};

module.exports = createsuppliers;
