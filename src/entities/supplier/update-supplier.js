const updatesupplier = ({}) => {
  return function updatesuppliername({
    company_name,
    contact_no,
    company_address,
    status,
    updated_at,
    updated_by,
    supplier_id,
  }) {
    if (!company_name) {
      throw new Error("Please Input company_name");
    }
    if (!contact_no) {
      throw new Error("Please input contact_no");
    }

    if (!company_address) {
      throw new Error("Please input company_address");
    }

    return Object.freeze({
      updateCompanyName: () => company_name,
      updateAddress: () => company_address,
      updateContactNumber: () => contact_no,
      updateStatus: () => status,
      updated_At: () => updated_at,
      updated_By: () => updated_by,
    });
  };
};

module.exports = updatesupplier;
