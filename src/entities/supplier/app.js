const getsuppliers = require("./supplier");
const createsupplier = require("./create-supplier");
const updatesupplier = require("./update-supplier");

const getsupplier = getsuppliers({});
const makesupplier = createsupplier({});
const updatesuppliername = updatesupplier({});

const accountEntity = Object.freeze({
  getsupplier,
  makesupplier,
  updatesuppliername,
});
module.exports = accountEntity;
module.exports = {
  getsupplier,
  makesupplier,
  updatesuppliername,
};
