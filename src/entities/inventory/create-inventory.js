const createinventory = ({ }) => {
 
    return function createinventory({ barcode,product_description,quantity,unit_cost,sales_cost,created_at,created_by}) {
            if (!barcode) {
                throw new Error("Please Input barcode");
            }
            if (!product_description) {
              throw Error("Please input product_description");
            }

            if (!quantity) {
                throw new Error("Please input quantity");
            }
        
            if (!unit_cost) {
                throw new Error("Please input unit_cost");
            }
 
            if (!sales_cost) {
                throw new Error("Please input sales_cost");
            }

            if (!created_at) {
                throw new Error("Please input created_at");
            }
 
            if (!created_by) {
                throw new Error("Please input created_by");
            }

            return Object.freeze({
                GetBarcode: () => barcode,
                GetProductDes: () => product_description,
                GeTQuantity: () => quantity,
                GetUnitcost: () => unit_cost,
                GetSalecost: () => sales_cost,
                GetCreated_At: () => created_at,
                GetCreated_By: () => created_by,


            })



    }
}

module.exports = createinventory;