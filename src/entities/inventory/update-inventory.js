const updateinventorys = ({}) => {
  return function updateinventoryname({
    product_description,
    quantity,
    barcode,
    unit_cost,
    sales_cost,
    updated_by,
    updated_at,
  }) {
    if (!product_description) {
      throw new Error("Please Input product_description");
    }
    // if (!updated_at) {
    //   throw new Error("Please input updated_at");
    // }

    // if (!updated_by) {
    //     throw new Error("Please input updated_by");
    // }

    return Object.freeze({
      updateProdectDescript: () => product_description,
      updateBarcode: () => barcode,
      updateQuantity: () => quantity,
      updateUnitCost: () => unit_cost,
      updateSalesCost: () => sales_cost,
      updateBY: () => updated_by,
      updateAt: () => updated_at,
    });
  };
};

module.exports = updateinventorys;
