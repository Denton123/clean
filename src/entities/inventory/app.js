const getinventorylist = require("./inventory");
const createinventories = require("./create-inventory");
const updateinventory = require("./update-inventory");

const getinventory = getinventorylist({});
const makeinventory = createinventories({});
const updateinventoryname = updateinventory({});

const accountEntity = Object.freeze({
  getinventory,
  makeinventory,
  updateinventoryname,
});

module.exports = accountEntity;
module.exports = {
  getinventory,
  makeinventory,
  updateinventoryname,
};
