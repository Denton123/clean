const createroles = ({ }) => {
 
    return function makerole({ role_name,created_at,created_by}) {
            if (!role_name) {
                throw new Error("Please Input Role Name");
                
            }
            if (!created_at) {
              throw new Error("Please input created_at");
            }

            if (!created_by) {
                throw new Error("Please input created_by");
            }
        

            return Object.freeze({
                getrolename: () => role_name,
                getcreated_at: () => created_at,
                getcreated_by: () => created_by
               
            })
     
    }
}

module.exports = createroles;