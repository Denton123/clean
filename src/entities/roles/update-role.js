const updaterole = ({}) => {
  return function updaterolename({ role_name, updated_at, updated_by }) {
    if (!role_name) {
      throw new Error("Please Input Role Name");
    }
    // if (!updated_at) {
    //   throw new Error("Please input updated_at");
    // }

    // if (!updated_by) {
    //     throw new Error("Please input updated_by");
    // }

    return Object.freeze({
      updateRolename: () => role_name,
      updateAt: () => updated_at,
      updateBy: () => updated_by,
    });
  };
};

module.exports = updaterole;
