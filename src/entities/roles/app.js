const getrolelist = require('./roles');
const createroles = require('./create-role');
const updaterole = require('./update-role');

const getroles = getrolelist({});
const makerole = createroles({});
const updaterolename = updaterole({});

const accountEntity = Object.freeze({
    getroles,
    makerole,
    updaterolename

});

module.exports = accountEntity;
module.exports = {
    getroles,
    makerole,
    updaterolename

 
}