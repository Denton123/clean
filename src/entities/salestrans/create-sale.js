const createssale = ({ }) => {
 
    return function createsales({ total_cost,payment_amt,customer_name,customer_address,quantity,customer_contact_no,created_by,inventory_id,items}) {
    
            if (!total_cost) {
              throw new Error("Please input total_cost");
            }

            if (!payment_amt) {
              throw new Error("Please input payment_amt");
            }


          
            if (!created_by) {
              throw new Error("Please input created_by");
            }


  
            return Object.freeze({
              GetTotalCost: () => total_cost,
              getPayment: () => payment_amt,
              GetCreated_by: () => created_by,
              GetCustomerName: () => customer_name,
              GetCustomerNumber: () => customer_contact_no,
              GetCustomAddress: () => customer_address,
              GetQuantity: () => quantity,
              
              // GetCostPerUnit: () => inventory_id,
              getItems: () => items
             
             
          })
   

 
     
        
    }
}

module.exports = createssale;