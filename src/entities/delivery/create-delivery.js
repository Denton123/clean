const createdeliveries = ({ }) => {
 
    return function createdelivery({ supplier_id,total_cost,dr_no,created_by,quantity,cost_per_unit, items}) {
    
            if (!supplier_id) {
              throw new Error("Please input Supplier");
            }

            if (!total_cost) {
              throw new Error("Please input Total Delivery Transaction Amount.");
            }
         
            if (!dr_no) {
              throw new Error("Please input Delivery Receipt Number");
            }

            if (!created_by) {
              throw new Error("Please input Delivery Receipt Number");
            }

             if (!items) {
              throw new Error("Please provide items.");
            }
        
            // if (!quantity) {
            //   throw new Error("Please input quantity");
            // }

            // if (!cost_per_unit) {
            //   throw new Error("Please input item.");
            // }
     

            
            return Object.freeze({
              GetSupplierID: () => supplier_id,
              GetTotalCost: () => total_cost,
              GetDr: () => dr_no,
              GetCreated_by: () => created_by,
              GetQuantity: () => quantity,
              GetCostPerUnit: () => cost_per_unit,
              getItems: () => items
             
             
          })
   
        
    }
}

module.exports = createdeliveries;