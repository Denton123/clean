const { DeliveryDB } = require('../../data-access/delivery/app');
const CON_getdelivery = require('./delivery');
const CON_createdelivery = require('./create-delivery');


const getdelivery = CON_getdelivery({ DeliveryDB }); 
const createdelivery= CON_createdelivery({ DeliveryDB }); 

const accountService = Object.freeze({
    getdelivery,
    createdelivery
});

module.exports = accountService;
module.exports = {
    getdelivery,
    createdelivery
 

    
}