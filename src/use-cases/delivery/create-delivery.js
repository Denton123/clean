const { makedelivery } = require("../../entities/delivery/app");

const UC_createdelivery = ({ DeliveryDB }) => {
  return async function createdelivery(DeliveryInfo) {
    const accountEntity = makedelivery(DeliveryInfo);
    const check = await DeliveryDB.finddelivery(accountEntity.GetDr());
console.log(check)
    if (check.rowCount != 0) {
      throw new Error("DR already exists");
    }

    const info = {
      supplier_id: accountEntity.GetSupplierID(),
      total_cost: accountEntity.GetTotalCost(),
      dr_no: accountEntity.GetDr(),
      created_by: accountEntity.GetCreated_by(),
      quantity: accountEntity.GetQuantity(),
      cost_per_unit: accountEntity.GetCostPerUnit(),
      items: accountEntity.getItems(),
    };

    const delivery = await DeliveryDB.createdelivery(info);

    return delivery.rows;
  };
};

module.exports = UC_createdelivery;
