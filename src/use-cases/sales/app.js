const { saleDB } = require('../../data-access/salestrans/app');
const CON_getsale = require('./salestrans');
const CON_createsale = require('./create-sale');



const getsale = CON_getsale({ saleDB }); 
const createsale= CON_createsale({ saleDB }); 

const accountService = Object.freeze({
    getsale,
    createsale
});

module.exports = accountService;
module.exports = {
    getsale,
    createsale
 

    
}