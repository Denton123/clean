const { makesale} = require('../../entities/salestrans/app');

const UC_createsales= ({ saleDB }) => {
    return async function createsales(saleInfo) {
        const accountEntity = makesale(saleInfo);
        // const check = await saleDB.createsales(saleInfo);

    // if (check.rowCount != 0) {
    //   throw new Error("DR already exists");
    // }
  
      const info = {
  
        total_cost: accountEntity.GetTotalCost(),
        payment_amt: accountEntity.getPayment(),
        customer_name: accountEntity.GetCustomerName(),
        customer_contact_no: accountEntity.GetCustomerNumber(),
        created_by: accountEntity.GetCreated_by(),
        quantity: accountEntity.GetQuantity(),
        customer_address: accountEntity.GetCustomAddress(),
        
        items: accountEntity.getItems(),
      };

      const sales = await saleDB.createsales(info);
      // console.log(sales)
      return sales.rows;
    };
}

module.exports = UC_createsales;