const { getroles } = require('../../entities/roles/app');

const UC_getAccount = ({ roleDb }) => {
    return async function getrole(accountInfo) {
        const accountEntity = getroles(accountInfo);
        const checkAccount = await roleDb.listAccounts();

        // console.log(checkAccount)
        // if (Object.entries(checkAccount).length != 0) {
        //     throw new Error("Account with Employee ID already exists.")
        // }

        // return accountDb.createAccount(
        //     accountEntity.getUserid(),
        //     accountEntity.getUsername(),
        //     accountEntity.getPassword(),
        //     accountEntity.getRole()
        // )

        return checkAccount.rows
    }
}

module.exports = UC_getAccount;