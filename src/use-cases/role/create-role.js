const { makerole } = require("../../entities/roles/app");

const UC_createrole = ({ roleDb }) => {
  return async function createroles(roleInfo) {
    const accountEntity = makerole(roleInfo);
    const check = await roleDb.findrole(accountEntity.getrolename());
    console.log(check.rows);
    if (check.rows.length != 0) {
      throw new Error("role_name already exists");
    }
    const info = {
      role_name: accountEntity.getrolename(),
      created_at: accountEntity.getcreated_at(),
      created_by: accountEntity.getcreated_by(),
    };

    return (await roleDb.createrole(info)).rows;
  };
};

module.exports = UC_createrole;
