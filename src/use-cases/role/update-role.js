const { updaterolename } = require("../../entities/roles/app");

const UC_createrole = ({ roleDb }) => {
  return async function updaterole(updateroleinfo, role_id) {
    const accountEntity = updaterolename(updateroleinfo);
    const check = await roleDb.findrole(accountEntity.updateRolename());
    console.log(check.rows);
    if (check.rows.length != 0) {
      const { role_name } = check.rows[0];
      throw new Error(`${role_name} already exists`);
    }
    const info = {
      role_name: accountEntity.updateRolename(),
      updated_at: accountEntity.updateAt(),
      updated_by: accountEntity.updateBy(),
    };

    return (await roleDb.updaterole(info, role_id)).rows;
  };
};

module.exports = UC_createrole;
