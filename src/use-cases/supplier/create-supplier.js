const { makesupplier } = require("../../entities/supplier/app");

const UC_createsuppliers = ({ supplierDB }) => {
  return async function createsuppliers(supplierinfo) {
    const accountEntity = makesupplier(supplierinfo);
    const check = await supplierDB.findsupplier(accountEntity.GetCompanyName());

    console.log(check.rows);
    if (check.rows.length != 0) {
      throw new Error("company_name already exist!");
    }

    const info = {
      company_name: accountEntity.GetCompanyName(),
      contact_no: accountEntity.GetContactNum(),
      company_address: accountEntity.GetContactAddress(),
      status: accountEntity.GetStatus(),
      created_at: accountEntity.GetCreatedAt(),
      created_by: accountEntity.GetCreatedBy(),
    };

    return (await supplierDB.createsupplier(info)).rows;
  };
};

module.exports = UC_createsuppliers;
