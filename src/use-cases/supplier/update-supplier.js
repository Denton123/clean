const { updatesuppliername } = require("../../entities/supplier/app");

const UC_updatesupplier = ({ supplierDB }) => {
  return async function updatesuppliers(updatesupplierinfo, supplier_id) {
    const accountEntity = updatesuppliername(updatesupplierinfo);
    const check = await supplierDB.findsupplier(
      accountEntity.updateCompanyName()
    );
    // console.log(check.rows);
    if (check.rows.length != 0) {
      const { company_name } = check.rows[0];
      throw new Error(` ${company_name} already exist!!`);
    }
    const info = {
      company_name: accountEntity.updateCompanyName(),
      company_address: accountEntity.updateAddress(),
      contact_no: accountEntity.updateContactNumber(),
      status: accountEntity.updateStatus(),
      updated_at: accountEntity.updated_At(),
      updated_by: accountEntity.updated_By(),
    };

    return (await supplierDB.updatesupplier(info, supplier_id)).rows;
  };
};

module.exports = UC_updatesupplier;
