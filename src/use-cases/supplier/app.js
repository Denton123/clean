const { supplierDB } = require("../../data-access/supplier/app");
const CON_getsupplier = require("./supplier");
const CON_createsupplier = require("./create-supplier");
const CON_updatesupplier = require("./update-supplier");

const getsupplier = CON_getsupplier({ supplierDB });
const createsupplier = CON_createsupplier({ supplierDB });
const updatesupplier = CON_updatesupplier({ supplierDB });

const accountService = Object.freeze({
  getsupplier,
  createsupplier,
  updatesupplier,
});

module.exports = accountService;
module.exports = {
  getsupplier,
  createsupplier,
  updatesupplier,
};
