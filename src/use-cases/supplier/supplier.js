const { getsupplier } = require("../../entities/supplier/app");

const UC_getsupplier = ({ supplierDB }) => {
  return async function getsuppliers(accountInfo) {
    const accountEntity = getsupplier(accountInfo);
    const checkAccount = await supplierDB.listAccounts();

    return checkAccount.rows;
  };
};

module.exports = UC_getsupplier;
