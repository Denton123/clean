const { updateusername } = require("../../entities/user/app");

const UC_updateusers = ({ accountDb }) => {
  return async function updateusers(userinfo, users_id) {
    const accountEntity = updateusername(userinfo);
    const check = await accountDb.findname(accountEntity.updatename());
    // console.log(check.rows);
    if (check.rows.length != 0) {
      const { username } = check.rows[0];
      throw new Error(` ${username} already exist!!`);
    }
    const info = {
      username: accountEntity.updatename(),
      password: accountEntity.updatePassword(),
      role_id: accountEntity.updateRole(),
      updated_by: accountEntity.updateBY(),
      updated_at: accountEntity.updateAt(),
    };

    return await accountDb.updateusers(info, users_id);
  };
};

module.exports = UC_updateusers;
