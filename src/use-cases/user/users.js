const { getuser } = require("../../entities/user/app");

const UC_getAccount = ({ accountDb }) => {
  return async function getusers(accountInfo) {
    const accountEntity = getuser(accountInfo);
    const checkAccount = await accountDb.listAccounts();

    // console.log(checkAccount)
    // if (Object.entries(checkAccount).length != 0) {
    //     throw new Error("Account with Employee ID already exists.")
    // }

    // return accountDb.createAccount(
    //     accountEntity.getUserid(),
    //     accountEntity.getUsername(),
    //     accountEntity.getPassword(),
    //     accountEntity.getRole()
    // )

    return checkAccount.rows;
  };
};

module.exports = UC_getAccount;
