const { makeuser } = require("../../entities/user/app");

const UC_createusers = ({ accountDb }) => {
  return async function createusers(accountInfo) {
    const accountEntity = makeuser(accountInfo);
    const checkemployeecode = await accountDb.findemployeeCode(
      accountEntity.createEmployeeCode()
    );

    //employee_code
    if (checkemployeecode.rows.length != 0) {
      const { employee_code } = checkemployeecode.rows[0];
      throw new Error(`${employee_code} already exists`);
    }
    //username
    const checkename = await accountDb.findname(accountEntity.createUsename());
    if (checkename.rows.length != 0) {
      const { username } = checkename.rows[0];
      throw new Error(`${username} already exists`);
    }
    // console.log(checkename.rows);
    const info = {
      employee_code: accountEntity.createEmployeeCode(),
      username: accountEntity.createUsename(),
      password: accountEntity.createPassword(),
      role_id: accountEntity.roleID(),
      created_at: accountEntity.createdAt(),
      created_by: accountEntity.createdBy(),
    };
    console.log(checkename);
    return await accountDb.createusers(info);
  };
};

module.exports = UC_createusers;
