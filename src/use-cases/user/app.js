const { accountDb } = require("../../data-access/user/app");
const CON_getusers = require("./users");
const CON_createuser = require("./create-user");
const CON_updateuser = require("./update-user");

const getusers = CON_getusers({ accountDb });
const createuser = CON_createuser({ accountDb });
const updateuser = CON_updateuser({ accountDb });

const accountService = Object.freeze({
  getusers,
  createuser,
  updateuser,
});

module.exports = accountService;
module.exports = {
  getusers,
  createuser,
  updateuser,
};
