const { updateinventoryname } = require("../../entities/inventory/app");

const UC_createinventory = ({ invenDb }) => {
  return async function updateinventory(inventoryinfo, inventory_id) {
    const accountEntity = updateinventoryname(inventoryinfo);
    const check = await invenDb.listAccounts(
      accountEntity.updateProdectDescript()
    );
    console.log(check.rows);
    const info = {
      product_description: accountEntity.updateProdectDescript(),
      barcode: accountEntity.updateBarcode(),
      quantity: accountEntity.updateQuantity(),
      unit_cost: accountEntity.updateUnitCost(),
      sales_cost: accountEntity.updateSalesCost(),
      updated_by: accountEntity.updateBY(),
      updated_at: accountEntity.updateAt(),
    };
    // console.log(info);
    return (await invenDb.updateinventory(info, inventory_id)).rows;
  };
};

module.exports = UC_createinventory;
