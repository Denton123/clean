const { invenDb } = require("../../data-access/inventory/app");
const CON_getinventory = require("./inventory");
const CON_createinventory = require("./create-inventory");
const CON_updateinventory = require("./update-inventory");

const getinventory = CON_getinventory({ invenDb });
const createinventory = CON_createinventory({ invenDb });
const updateinventory = CON_updateinventory({ invenDb });

const accountService = Object.freeze({
  getinventory,
  createinventory,
  updateinventory,
});

module.exports = accountService;
module.exports = {
  getinventory,
  createinventory,
  updateinventory,
};
