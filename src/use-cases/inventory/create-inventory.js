const { makeinventory} = require('../../entities/inventory/app');

const UC_createinventory= ({ invenDb }) => {
    return async function createinventoryS(invfo) {
        const accountEntity = makeinventory(invfo);
        const checkinvo = await invenDb.findinvo(accountEntity.GetBarcode());
      
         
        console.log(checkinvo.rows)
        if (checkinvo.rows.length != 0) {
            throw new Error("barcode is already exist");
        }

        const invo = {
            barcode:accountEntity.GetBarcode(),
            product_description:accountEntity.GetProductDes(),
            quantity:accountEntity.GeTQuantity(),
            unit_cost: accountEntity.GetUnitcost(),
            sales_cost:accountEntity.GetSalecost(),
            created_at:accountEntity.GetCreated_At(),
            created_by:accountEntity.GetCreated_By()
     }


     return (await invenDb.createinventory(invo)).rows;
    }
}

module.exports = UC_createinventory;