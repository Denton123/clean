const CON_getroles = ({ getroles }) => {
  return async function getroless(httpRequest) {
    try {
      const { source = {}, ...accountInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const role_list = await getroles(accountInfo);

      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 200,
        body: {
          message: "Sucess.",
          role_list,
        },
      };
    } catch (err) {
      // console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_getroles;
