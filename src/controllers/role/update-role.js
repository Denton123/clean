const CON_updaterole = ({ updaterole }) => {
  return async function updateroles(httpRequest) {
    try {
      const { source = {}, ...updateroleinfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const { role_id } = httpRequest.params;
      const result = await updaterole(updateroleinfo, role_id);

      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 201,

        body: {
          msg: "updated role successfully",
          result,
        },
      };
    } catch (err) {
      console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_updaterole;
