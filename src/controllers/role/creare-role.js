const CON_createrole = ({ createrole }) => {
  return async function createrolee(httpRequest) {

    try {
      const { source = {}, ...roleInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const result = await createrole(roleInfo);

      return {
        headers: {
          "Constent-Type": "application/json",

        },
        statusCode: 201,
        body: {
          message: "Account has been created.",
          result
        }
      }

    } catch (err) {
      console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: err.message
        }
      }
    }
  }
}
module.exports = CON_createrole;