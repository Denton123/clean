const CON_getroles = require("./get-roles");
const { getroles } = require("../../use-cases/role/app");
const _getroles= CON_getroles({ getroles });

//create
const CON_createrole = require("./creare-role");
const { createrole } = require("../../use-cases/role/app");
const _createroles= CON_createrole({ createrole });

//update
const CON_updaterole = require("./update-role");
const { updaterole } = require("../../use-cases/role/app");
const _updaterole= CON_updaterole({ updaterole });



const CON_account = Object.freeze({
  _getroles,
  _createroles,
  _updaterole

});

module.exports = CON_account;
module.exports = {
  _getroles,
  _createroles,
  _updaterole

};
