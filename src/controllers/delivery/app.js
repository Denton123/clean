const CON_getdelivery = require("./delivery");
const {getdelivery } = require("../../use-cases/delivery/app");
const _getdelivery= CON_getdelivery({ getdelivery });


//create inventory
const CON_createdelivery = require("./create-delivery");
const { createdelivery } = require("../../use-cases/delivery/app");
const _createdelivery= CON_createdelivery({ createdelivery });



const CON_account = Object.freeze({
    _getdelivery,
    _createdelivery


});

module.exports = CON_account;
module.exports = {
    _getdelivery,
    _createdelivery

};
