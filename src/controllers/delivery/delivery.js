const CON_getdelivery= ({ getdelivery }) => {
    return async function getdeliverys(httpRequest) {
      try {
        const { source = {}, ...DeliveryInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-agent"];
  
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
  
        const delivery_list = await getdelivery(DeliveryInfo);
  
        return {
          headers: {
            "Constent-Type": "application/json",
          },
          statusCode: 200,
          body: {
            message: "Sucess.",
            delivery_list,
          },
        };
      } catch (err) {
        // console.log(err);
        return {
          headers: {
            "Constent-Type": "application/json",
          },
          statusCode: 400,
          body: {
            error: err.message,
          },
        };
      }
    };
  };
  module.exports = CON_getdelivery;
  