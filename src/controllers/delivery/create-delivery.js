const CON_createdelivery = ({ createdelivery }) => {
    return async function createdeliverys(httpRequest) {
  
      try {
        const { source = {}, ...DeliveryInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-agent"];

        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
  
        const result = await createdelivery(DeliveryInfo);
  
        return {
          headers: {
            "Constent-Type": "application/json",
  
          },
          statusCode: 201,
          body: {
            msg: "New delivery transaction added successfully",
            result
          }
        }
  
      } catch (err) {
        console.log(err);
        return {
          headers: {
            "Constent-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: err.message
          }
        }
      }
    }
  }
  module.exports = CON_createdelivery;