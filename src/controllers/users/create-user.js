const CON_createuser = ({ createuser }) => {
  return async function createusers(httpRequest) {
    try {
      const { source = {}, ...accountInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const result = await createuser(accountInfo);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 201,
        body: {
          message: "New users Added successfully",
          result,
        },
      };
    } catch (err) {
      console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_createuser;
