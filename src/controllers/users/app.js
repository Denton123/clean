const CON_getusers = require("./get-users");
const { getusers } = require("../../use-cases/user/app");
const _getusers = CON_getusers({ getusers });

//create users
const CON_createuser = require("./create-user");
const { createuser } = require("../../use-cases/user/app");
const _createuser = CON_createuser({ createuser });

//update users
const CON_updateuser = require("./update-user");
const { updateuser } = require("../../use-cases/user/app");
const _updateuser = CON_updateuser({ updateuser });

const CON_account = Object.freeze({
  _getusers,
  _createuser,
  _updateuser,
});

module.exports = CON_account;
module.exports = {
  _getusers,
  _createuser,
  _updateuser,
};
