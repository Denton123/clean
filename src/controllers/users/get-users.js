const CON_getusers = ({ getusers }) => {
  return async function getuserss(httpRequest) {
    try {
      const { source = {}, ...accountInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const users = await getusers(accountInfo);

      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 200,
        body: {
          message: "Sucess.",
          users,
        },
      };
    } catch (err) {
      // console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_getusers;
