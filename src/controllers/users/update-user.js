const CON_updateuser = ({ updateuser }) => {
  return async function updateusers(httpRequest) {
    try {
      const { source = {}, ...inventoryinfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const { users_id } = httpRequest.params;
      const result = await updateuser(inventoryinfo, users_id);

      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 201,

        body: {
          msg: " User updated successfully",
          result,
        },
      };
    } catch (err) {
      console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_updateuser;
