const CON_createsupplier = ({ createsupplier }) => {
  return async function createsuppliers(httpRequest) {
    try {
      const { source = {}, ...supplierinfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const result = await createsupplier(supplierinfo);

      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 201,
        body: {
          msg: "New Supplier Added successfully",
          result,
        },
      };
    } catch (err) {
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_createsupplier;
