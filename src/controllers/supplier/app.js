const CON_getsupplier = require("./get-supplier");
const { getsupplier } = require("../../use-cases/supplier/app");
const _getsupplier = CON_getsupplier({ getsupplier });

//create supplier
const CON_createsupplier = require("./create-supplier");
const { createsupplier } = require("../../use-cases/supplier/app");
const _createsupplier = CON_createsupplier({ createsupplier });

//updates
const CON_updatesupplier = require("./update-supplier");
const { updatesupplier } = require("../../use-cases/supplier/app");
const _updatesupplier = CON_updatesupplier({ updatesupplier });

const CON_account = Object.freeze({
  _getsupplier,
  _createsupplier,
  _updatesupplier,
});

module.exports = CON_account;
module.exports = {
  _getsupplier,
  _createsupplier,
  _updatesupplier,
};
