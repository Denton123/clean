const CON_updatesupplier = ({ updatesupplier }) => {
  return async function updatesupplierss(httpRequest) {
    try {
      const { source = {}, ...updatesupplierinfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const { supplier_id } = httpRequest.params;
      const result = await updatesupplier(updatesupplierinfo, supplier_id);

      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 201,

        body: {
          msg: "Supplier updated successfully",
          result,
        },
      };
    } catch (err) {
      console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_updatesupplier;
