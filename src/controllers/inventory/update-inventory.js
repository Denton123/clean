const CON_updateinventory = ({ updateinventory }) => {
  return async function updateinventorys(httpRequest) {
    try {
      const { source = {}, ...inventoryinfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      const { inventory_id } = httpRequest.params;
      const result = await updateinventory(inventoryinfo, inventory_id);

      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 201,

        body: {
          msg: "inventory updated  successfully",
          result,
        },
      };
    } catch (err) {
      console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_updateinventory;
