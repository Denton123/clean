const CON_getinventoy= ({ getinventory }) => {
    return async function getinventorys(httpRequest) {
      try {
        const { source = {}, ...invenInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-agent"];
  
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
  
        const inventory_list = await getinventory(invenInfo);
  
        return {
          headers: {
            "Constent-Type": "application/json",
          },
          statusCode: 200,
          body: {
            message: "Sucess.",
            inventory_list,
          },
        };
      } catch (err) {
        // console.log(err);
        return {
          headers: {
            "Constent-Type": "application/json",
          },
          statusCode: 400,
          body: {
            error: err.message,
          },
        };
      }
    };
  };
  module.exports = CON_getinventoy;
  