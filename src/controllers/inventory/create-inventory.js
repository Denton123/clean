const CON_createinventory = ({ createinventory }) => {
  return async function createinventorye(httpRequest) {
    try {
      const { source = {}, ...invfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-agent"];

      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }


      const result = await createinventory(invfo);
      console.log(result)
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 201,
        body: {
          msg: "New inventory Added successfully",
          result,
        },
      };
    } catch (err) {
      console.log(err);
      return {
        headers: {
          "Constent-Type": "application/json",
        },
        statusCode: 400,
        body: {
          error: err.message,
        },
      };
    }
  };
};
module.exports = CON_createinventory;
