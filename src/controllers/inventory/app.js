const CON_getinventory = require("./get-inventory");
const { getinventory } = require("../../use-cases/inventory/app");
const _getinventory = CON_getinventory({ getinventory });

//create inventory
const CON_createinventory = require("./create-inventory");
const { createinventory } = require("../../use-cases/inventory/app");
const _createinventory = CON_createinventory({ createinventory });

//updates
const CON_updateinventory = require("./update-inventory");
const { updateinventory } = require("../../use-cases/inventory/app");
const _updateinventory = CON_updateinventory({ updateinventory });

const CON_account = Object.freeze({
  _getinventory,
  _createinventory,
  _updateinventory,
});

module.exports = CON_account;
module.exports = {
  _getinventory,
  _createinventory,
  _updateinventory,
};
