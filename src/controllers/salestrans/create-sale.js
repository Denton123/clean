const CON_createsale = ({ createsale }) => {
    return async function createsales(httpRequest) {
     
      try {
        const { source = {}, ...saleInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-agent"];

        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
  
 
        const result = await createsale(saleInfo);
        // console.log()
        return {
          headers: {
            "Constent-Type": "application/json",
  
          },
          
          statusCode: 201,
          
          body: {
            
            msg: "New sales transaction added successfully",
            result
          }
        }
  
      } catch (err) {
        console.log(err);
        return {
          headers: {
            "Constent-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: err.message

          }
        }
      }
    }
  }
  module.exports = CON_createsale;