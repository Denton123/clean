const CON_getsale= ({ getsale }) => {
    return async function getsales(httpRequest) {
      try {
        const { source = {}, ...saleInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-agent"];
  
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
  
        const sales = await getsale(saleInfo);
  
        return {
          headers: {
            "Constent-Type": "application/json",
          },
          statusCode: 200,
          body: {
            message: "Sucess.",
            sales,
          },
        };
      } catch (err) {
        // console.log(err);
        return {
          headers: {
            "Constent-Type": "application/json",
          },
          statusCode: 400,
          body: {
            error: err.message,
          },
        };
      }
    };
  };
  module.exports = CON_getsale;
  