let jwt = require("jsonwebtoken");
const config = require("./config.js");

module.exports = {
  verifytoken(req, res, next) {
    let token = req.headers["x-access-token"] || req.headers["authorization"];

    if (token) {
      if (token.startsWith("Bearer ")) {
        token = token.slice(7, token.length);
      }

      jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
          return res.sendStatus(403);
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      return res.sendStatus(403);
    }
  }
};
